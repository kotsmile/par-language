# **Par** concatinative deque based language

## Stages

- [ ] Tokenizer
- [ ] Lexer
- [ ] Parser

## Example

1. Standard stuff

```
1 2 + print # 3
```

2. Also can access to left part of `deque`

```
1! 2! +! print # 3
```
